#include "Helpers.h"

int squareOfTheSum(int a, int b)
{
	int sum = a + b;
	return sum * sum;
}